package wxformbuilder_project.editors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.*;

import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.win32.W32APIOptions;


import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

/*!
 * Object used for fetching a window handle for a specified process id.
 * 
 * @class	WindowEnumerationResult
 */


class WindowEnumerationResult {
	/*!
	 * The process identifier to find the window handle of.
	 * 
	 * @var	_processId
	 */
	DWORD _processId;
	
	/*!
	 * The resulting window handle.
	 * 
	 * @var	_windowHandle
	 */
	HWND _windowHandle;
	
	/*!
	 * Constructor for a new enumeration window result for a specified process id.
	 * 
	 * @var	processId	The process id to fetch the window of.
	 */
	public WindowEnumerationResult(DWORD processId) {
		
		_processId = processId;
	}
	
	/*!
	 * Returns the process id to find.
	 * 
	 * @return	The receiver's process id.
	 */
	public DWORD processId() {
		
		return _processId;
	}
	
	/*!
	 * Sets the window handle found.
	 * 
	 * @var	handle	The new window handle.
	 */
	public void setWindowHandle(HWND handle) {
		
		_windowHandle = handle;
	}
	
	/*!
	 * Returns the found window handle or null.
	 * 
	 * @return	Null when the window is not found otherwise the window handle.
	 */
	public HWND windowHandle() {
		
		return _windowHandle;
	}
};

/*!
 * The editor for wxFormBuilder files.
 * 
 * @class	wxFormBuilderEditor
 */
public class wxFormBuilderEditor extends EditorPart {

	private static boolean WINDOWS = false;
	/*!
	 * The name of the executable of the wxFormBuilder application.
	 * 
	 * @var	_formBuilderExecutableName
	 */

	private String FORM_BUILDER_EXECUTABLE_NAME	;

		/*!
	 * The form builder application window handle to use in the composite.
	 * 
	 * @var	_formBuilderWindowHandle
	 */
	private HWND _formBuilderWindowHandle;
	
	/*!
	 * The child window handler which is set when the window is successfully moved.
	 * 
	 * @var	_childFormBuilderWindowHandle
	 */
	private HWND _childFormBuilderWindowHandle;
	
	/*!
	 * Returns the process id for the wxFormBuilder process.
	 * 
	 * @param	executableName	The name of the executable to find.
	 * @return	The process id of the found process or 0 when the process could not be found.
	 */
	private DWORD processIdForExectuableName(String executableName) {
		
		// default return value
		DWORD returnValue = new DWORD(-1);
		
		if(WINDOWS){ // BEGIN WINDOWS CODE
			
				// load library for process information
		Kernel32 kernel32 = (Kernel32) Native.loadLibrary(Kernel32.class, W32APIOptions.UNICODE_OPTIONS);
        Tlhelp32.PROCESSENTRY32.ByReference processEntry = new Tlhelp32.PROCESSENTRY32.ByReference();          

        // used for snapshots
        WinNT.HANDLE snapshot = kernel32.CreateToolhelp32Snapshot(Tlhelp32.TH32CS_SNAPPROCESS, new WinDef.DWORD(0));
        try  {
        	
        	// keep processing all processes until the correct one is found
            while (kernel32.Process32Next(snapshot, processEntry)) {    
            	if (Native.toString(processEntry.szExeFile).equals(executableName)) {
            		
            		// the process is running so set the process id and break
            		returnValue = processEntry.th32ProcessID;
            		break;
            	}
            }
        } finally {
            kernel32.CloseHandle(snapshot);
        }
        
		} // END WINDOWS CODE
		
		return returnValue;
	}
	
	/*!
	 * Enumerate all open windows currently available
	 * 
	 * @param	pid	The process ID to find the window of.
	 * @return		A window handle of the process.
	 */
	public HWND getWindowHandleForProcessId(DWORD pid) {
	      
	      // create the result type which the window handle will be copied to
	      final WindowEnumerationResult handleResult = new WindowEnumerationResult(pid);
	      final User32 user32 = User32.INSTANCE;

	      // enumerate all available windows
	      user32.EnumWindows(new WNDENUMPROC() {
	         @Override
	         public boolean callback(HWND hWnd, Pointer arg1) {

	        	 // create a new int by reference which the result is copied to, must contain an initial value
	        	IntByReference integer = new IntByReference(1337);
	            user32.GetWindowThreadProcessId(hWnd, integer);
	            
	            // compare process identifiers
	            if (integer.getValue() == handleResult.processId().intValue()) {
	            	
	            	// copy the window text title
	            	char[] outputBuffer = new char[256];
	            	user32.GetWindowText(hWnd, outputBuffer, 256);
	            	
	            	String compareBuffer = "TEST - wxFormBuilder v3.0";
	            	String output = new String(outputBuffer);
	            	
	            	if (output.startsWith(compareBuffer)) {
		            	handleResult.setWindowHandle(hWnd);
	            	} 
	            }

	            return true;
	         }
	      }, null);
	      
	      // return the window handle when found
	      return handleResult.windowHandle();
	   }

	/*!
	 * Constructor which checks for the form builder executable.
	 */
	public wxFormBuilderEditor() {
		if (System.getProperty("os.name").equals("Linux"))
			WINDOWS=false;
		else
			WINDOWS=true;
				
		if(WINDOWS)
			FORM_BUILDER_EXECUTABLE_NAME=new String("wxFormBuilder.exe");
		else
			FORM_BUILDER_EXECUTABLE_NAME=new String("wxformbuilder");
		
		// get the process id for the formbuilder application
		DWORD pid = processIdForExectuableName(FORM_BUILDER_EXECUTABLE_NAME);
		System.out.println("Process ID found: " + pid.intValue());
		
		// check whether a window is available for the form builder
		if (pid.intValue() != -1) {
			
			// fetch the window handle of the form builder
			HWND windowHandle = getWindowHandleForProcessId(pid);
			
			// set the window handle when available
			if (windowHandle != null) {
				_formBuilderWindowHandle = windowHandle;
			}
		}
	}
	
	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createPartControl(Composite parent) {
	if(WINDOWS){ //begin windows code
		// only move the window when it is found
		if (this._formBuilderWindowHandle != null) {
		
			// create composite to add the child window to
			User32 user32 = User32.INSTANCE;
			Composite newComp = new Composite(parent, SWT.EMBEDDED);
			WinDef.HWND compositeHandle = new WinDef.HWND(new Pointer(newComp.handle));
			
			// first change the style of the window before moving it
			long style = user32.GetWindowLong(this._formBuilderWindowHandle, WinUser.GWL_STYLE);
			style |= WinUser.WS_CHILD;
			style &= ~WinUser.WS_POPUP;
			
			// append the style of the window
			user32.SetWindowLong(this._formBuilderWindowHandle, WinUser.GWL_STYLE, (int) style);
			
			// move the FormBuilder window to the new composite window of the editor
			this._childFormBuilderWindowHandle = User32.INSTANCE.SetParent(this._formBuilderWindowHandle, compositeHandle);
			
			if (this._childFormBuilderWindowHandle == null) {
				System.out.println("Failed to acquire window handle of composite, error code: " + Kernel32.INSTANCE.GetLastError());
			}
		} else {
			
			System.out.println("The wxFormBuilder is not available, make sure you have opened the application. (Could be still running in the process list, this one should be closed.)");
		}
	} // end windpows code
	
	else{ //[start linux code]
		Process p = null;
		try {
			p=Runtime.getRuntime().exec(FORM_BUILDER_EXECUTABLE_NAME);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		String WXFWindowId = null;
		try {
			Thread.sleep(3000); //ugly wait for window ready
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} //ugly wait to window wxformbuilder is ready
		
	   	try {
		    String line;
		    String[] cmd = {"bash", "-c", "xwininfo -root -tree | grep untitled | awk '{print $1}'"};
		    p = Runtime.getRuntime().exec(cmd);
		    BufferedReader in =
		            new BufferedReader(new InputStreamReader(p.getInputStream()));
		    while ((line = in.readLine()) != null) {
		        System.out.println("wxFormBuilderWindowId: "+line); 
		        WXFWindowId = line;
		    }
		    in.close();
		} catch (Exception ex) {
		    ex.printStackTrace();
		}
	   
	   	Composite newComp = new Composite(parent, SWT.EMBEDDED);
	   	String EclipseWID = "0x"+Long.toHexString(newComp.handle);
	   	System.out.println(WXFWindowId+"|"+EclipseWID);
	   	String[] cmd2 = {"bash", "-c", "xdotool windowreparent "+WXFWindowId+" "+EclipseWID};
		try {
			p = Runtime.getRuntime().exec(cmd2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} //end linux code
	}
	
	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	public void dispose() {
		
		// when the child window is available and not null close this window
		/*[windows]
		if (this._childFormBuilderWindowHandle != null) {
		 
			User32.INSTANCE.CloseWindow(this._childFormBuilderWindowHandle);
			
			// close the FormBuilder application
		}
		*/
		// dispose using super implementation
		super.dispose();
	}

	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub
		
	}

	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	public void doSaveAs() {
		// TODO Auto-generated method stub
		
	}

	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
				
		System.out.println(input.getClass());
		
		// set the site and input
		setSite(site);
		setInput(input);
	}

	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isDirty()
	 */
	public boolean isDirty() {

		return false;
	}

	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	public boolean isSaveAsAllowed() {
		
		return false;
	}

	/*!
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	public void setFocus() {
		
	}
}
